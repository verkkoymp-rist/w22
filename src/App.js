import './App.css';
import ComponentName from './ComponentName';

function App() {
  return (
    <div>
      <ComponentName country="Finland" />
      <ComponentName country="Norway" />
    </div>
  );
}

export default App;
